<%
    /**
     * 可以直接使用的内置对象：
     * jsonConfig 为前台录入的json参数转换后的对象；可以以json的方式直接读取；
     * scriptContext 为和molicode进行脚本传递的上下文；所有闭包必须在最后传递到scriptContext里面；
     * 工具类：
     * JSON（FastJSON 工具类）
     * StringUtils(apache commons 3), CollectionUtils(apache collections 4)
     * PubUtils, tableNameUtil 系统自带；
     */

    def jsonConfigLocal = jsonConfig;

    /**
     * 必须实现
     *
     * 代码片段开始查找器，返回true，意味着找到了开始行
     * 以行为单位逐行进行查找，参数为：
     * (String line, List<String> lineContentList, int lineIndex)
     * 其中 line 为当前行内容， lineContentList 为所有行内容， lineIndex 为行标（0开始）
     *
     */
    def segmentStartFinder = { line, lineContentList, lineIndex ->
        return line.contains(jsonConfigLocal['segmentStart']);
    }

    /**
     * 必须实现
     *
     * 代码片段结束查找器，返回true，意味着找到了结束行
     * 以行为单位逐行进行查找，参数为：
     * (String line, List<String> lineContentList, int lineIndex)
     * 其中 line 为当前行内容， lineContentList 为所有行内容， lineIndex 为行标（0开始）
     *
     */
    def segmentEndFinder = { line, lineContentList, lineIndex ->
        return line.contains(jsonConfigLocal['segmentEnd']);
    }

    /**
     * 可选实现，如果没有过滤器，只要在开始结束范围内的，均为满足条件的代码片段
     *
     * 代码片段过滤器，返回true，意味匹配，false以为不匹配；
     *
     * 参数为： (SegmentInfoBo segmentInfoBo)
     * SegmentInfoBo 对象暂未提供说明，请看原始代码；
     *
     */
    def segmentFilter = { segmentInfoBo ->
        def segmentContains = jsonConfigLocal['segmentContains'];
        if (segmentContains == null || segmentContains == '') {
            return true;
        }
        def originContent = segmentInfoBo.getOriginSegmentContent("")
        return originContent.contains(segmentContains);
    }

    /**
     * 必须实现
     *
     * 代码片段处理执行器，会按返回结果替换掉原来的代码片段；
     *
     * (SegmentInfoBo segmentInfoBo)
     * SegmentInfoBo 对象暂未提供说明，请看原始代码；
     *
     */
    def segmentProcess = { segmentInfoBo ->
        def originContent = segmentInfoBo.getOriginSegmentContent("")
        def replaceMap = jsonConfigLocal['replaceMap'];
        if (replaceMap == null || replaceMap.isEmpty()) {
            return originContent;
        }
        replaceMap.each { key, value ->
            originContent.replaceAll(key, value);
        }
        return originContent;
    }

    /**
     * 将以上处理器传递给molicode进行运算处理；
     */
    scriptContext['segmentStartFinder'] = segmentStartFinder;
    scriptContext['segmentEndFinder'] = segmentEndFinder;
    scriptContext['segmentFilter'] = segmentFilter;
    scriptContext['segmentProcess'] = segmentProcess;

%>
说明：用于进行常用的文本替换功能。smartSegment脚本初始化成功！
需要配合jsonConfig使用；
author: david
version: 1.0
