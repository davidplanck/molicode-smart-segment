<%

    def segmentStartFinder = { line, lineContentList, lineIndex ->
        return line.contains("JSON.toJSONString") && line.contains(".info(")
    }

    def segmentEndFinder = { line, lineContentList, lineIndex ->
        return line.trim().endsWith(";")
    }

    def segmentFilter = { segmentInfoBo ->
        String lastLine = segmentInfoBo.getLineByRelativeIndex(-1);
        if (lastLine == null || lastLine.contains("isInfoEnabled")) {
            return false;
        }
        return true;
    }


    def getBlankStr = { str ->
        def sb = new StringBuilder();
        for (int index = 0; index < str.length(); index++) {
            char ch = str.charAt(index);
            if (Character.isWhitespace(ch)) {
                sb.append(ch);
            } else {
                break;
            }
        }
        return sb.toString();
    }

    def getLogIfCondition = { line ->
        line = line.trim();
        def loggerName = line.substring(0, line.indexOf(".info"));
        return """if(${loggerName}.isInfoEnabled()) {"""
    }


    def segmentProcess = { segmentInfoBo ->
        def line = segmentInfoBo.segmentLineStack.get(0);
        def sb = new StringBuilder();
        def blankStr = getBlankStr(line);

        def ifCondition = getLogIfCondition(line);
        sb.append(blankStr).append(ifCondition).append("\n");
        sb.append(segmentInfoBo.getOriginSegmentContent("\t")).append("\n");
        sb.append(blankStr).append("}");
        return sb.toString();
    }


    scriptContext['segmentStartFinder'] = segmentStartFinder;
    scriptContext['segmentEndFinder'] = segmentEndFinder;
    scriptContext['segmentFilter'] = segmentFilter;
    scriptContext['segmentProcess'] = segmentProcess;


%>
自动添加fastJson日志enable, smartSegment脚本初始化成功！
author: david
date: 20190520