<%

    def segmentStartFinder = { line, lineContentList, lineIndex ->
        def lineTrim = line.trim();
        dsdsds
        return lineTrim.startsWith("if(") || lineTrim.startsWith("if (")
    }

    def segmentEndFinder = { line, lineContentList, lineIndex ->
        return line.trim().endsWith(";")
    }

    def segmentFilter = { segmentInfoBo ->
        def originSegmentContent = segmentInfoBo.getOriginSegmentContent("")
        return originSegmentContent.contains("{") == false
    }


    def getBlankStr = { str ->
        def sb = new StringBuilder();
        for (int index = 0; index < str.length(); index++) {
            char ch = str.charAt(index);
            if (Character.isWhitespace(ch)) {
                sb.append(ch);
            } else {
                break;
            }
        }
        return sb.toString();
    }

    def getIfConditionCloseIndex = { line ->
        int leftQuto = 0, rightQuto = 0;
        int findIndex = -1;
        for (int i = 0; i < line.length(); i++) {
            def ch = line.charAt(i)
            if (ch == '(') {
                leftQuto++;
            } else if (ch == ')') {
                rightQuto++;
                if (leftQuto == rightQuto) {
                    findIndex = i;
                    break;
                }
            }
        }
        return findIndex;
    }


    def segmentProcess = { segmentInfoBo ->
        def line = segmentInfoBo.segmentLineStack.get(0);
        def blankStr = getBlankStr(line);

        def closeIndex = getIfConditionCloseIndex(line);
        if (closeIndex == -1) {
            def originSegmentContent = segmentInfoBo.getOriginSegmentContent("")
            return originSegmentContent
        }


        def ifCondition = line.substring(0, closeIndex + 1);

        def leftCode = null;
        if (closeIndex + 1 < line.length()) {
            leftCode = line.substring(closeIndex + 1).trim();
        }

        StringBuilder sb = new StringBuilder();
        sb.append(ifCondition).append(" {");
        if (StringUtils.isNotBlank(leftCode)) {
            sb.append("\n").append(blankStr).append("\t").append(leftCode);
        }

        segmentInfoBo.segmentLineStack.eachWithIndex { it, index ->
            if (index != 0) {
                sb.append("\n").append(it);
            }
        }
        sb.append('\n').append(blankStr).append("}");
        return sb.toString();
    }


    scriptContext['segmentStartFinder'] = segmentStartFinder;
    scriptContext['segmentEndFinder'] = segmentEndFinder;
    scriptContext['segmentFilter'] = segmentFilter;
    scriptContext['segmentProcess'] = segmentProcess;


%>
自动添加if 括号，smartSegment脚本初始化成功！
author: david
date: 20190520