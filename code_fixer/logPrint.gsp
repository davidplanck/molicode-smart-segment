<%
    //def logger = com.shareyi.molicode.common.utils.LogHelper.FRONT_CONSOLE;
    def segmentStartFinder = { line, lineContentList, lineIndex ->
        return line.contains(".error(") || line.contains(".info(")
    }

    def segmentEndFinder = { line, lineContentList, lineIndex ->
        return line.trim().endsWith(";")
    }

    def segmentFilter = { segmentInfoBo ->
        String segmentContent = segmentInfoBo.getOriginSegmentContent("");
        def outQuotFlag = true;
        def plusFlagCnt = 0;
        def hasAppendStr = false;
        for (int index = 0; index < segmentContent.length(); index++) {
            char ch = segmentContent.charAt(index);
            if (ch == '"') {
                outQuotFlag = !outQuotFlag;
                if (plusFlagCnt > 0) {
                    hasAppendStr = true;
                }
            } else if (ch == '+' && outQuotFlag) {
                plusFlagCnt++;
            }

        }
        //目前只处理为1的情况
        return plusFlagCnt == 1 && !hasAppendStr;
    }

    /**
     * 获取表达式结尾符号
     */
    def getConditionCloseIndex = { line ->
        int leftQuto = 0, rightQuto = 0;
        int findIndex = -1;
        for (int i = 0; i < line.length(); i++) {
            def ch = line.charAt(i)
            if (ch == '(') {
                leftQuto++;
            } else if (ch == ')') {
                rightQuto++;
                if (leftQuto == rightQuto) {
                    findIndex = i;
                    break;
                }
            }
        }
        if (findIndex == -1) {
            def tempIndex = line.lastIndexOf(')');
            if (tempIndex > -1) {
                findIndex = tempIndex - 1;
            }
        }
        return findIndex;
    }

    def segmentProcess = { segmentInfoBo ->
        def segmentContent = segmentInfoBo.getOriginSegmentContent("");

        int lastQuotIndex = -1;
        char lastChar = ' ';
        def outQuotFlag = true;

        def subSegment = [:]
        for (int index = 0; index < segmentContent.length(); index++) {
            char ch = segmentContent.charAt(index);
            if (ch == '"' && (lastChar == null || lastChar != '\\')) {
                outQuotFlag = !outQuotFlag;
                lastQuotIndex = index;
            }
            if (ch == '+' && outQuotFlag) {
                subSegment['quotIndex'] = lastQuotIndex;
                subSegment['plusFlagIndex'] = index;
                int paramEndIndex = getConditionCloseIndex(segmentContent.substring(index))
                subSegment['paramEndIndex'] = index + paramEndIndex;
                break;
            }
            lastChar = ch;
        }

        def sb = new StringBuilder();
        sb.append(segmentContent.substring(0,subSegment['quotIndex'])).append("{}");
        sb.append("\",");
        sb.append(segmentContent.substring(subSegment['plusFlagIndex']+1));
       // logger.info("str={}",sb)
        return sb.toString();
    }


    scriptContext['segmentStartFinder'] = segmentStartFinder;
    scriptContext['segmentEndFinder'] = segmentEndFinder;
    scriptContext['segmentFilter'] = segmentFilter;
    scriptContext['segmentProcess'] = segmentProcess;


%>
日志打印优化项目, smartSegment脚本初始化成功！
author: david
date: 20190815

目前只能处理在末尾追加的参数；