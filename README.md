# molicode智能代码片段

#### 介绍
molicode智能代码片段，配合molicode代码工具，进行系统只能片段识别及智能文本处理；


### 使用说明

1. 下载本工程
2. 配合molicode工具使用；


### 资源说明

#### a. 代码修复 
位置：code_fixer 子文件目录下

文件说明：
1. ifCheck.gsp 脚本；修复Java代码if判断，没有大括号包裹的代码片段；
2. logEnable.gsp 脚本; 对info级别的日志，如果使用了FastJSON进行序列号，增加enable判断，减少性能损耗；
3. simpleReplace 脚本；通过前台配置json参数，对文本内容进行智能识别和替换；



### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目